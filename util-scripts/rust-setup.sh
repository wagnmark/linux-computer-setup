curl --proto '=https' --tlsv1.3 -sSf https://sh.rustup.rs | sh -s -- -y
# env setup
cargo install exa
cargo install cargo-bundle-licenses
cargo install cargo-cache
cargo install cargo-deb
cargo install cargo-deps
cargo install cargo-duplicates
cargo install cargo-edit
cargo install cargo-outdated
# dependencies
cargo install cargo-ui
cargo install tauri-cli
