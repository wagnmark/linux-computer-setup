git config --global user.name "Markus Wagner"
git config --global user.email "markus@hallo-wagners.de"
sudo apt install -y build-essential htop
flatpak install flathub com.visualstudio.code
sudo apt install -y openocd gcc-arm-none-eabi gcc-riscv64-unknown-elf can-utils cmake gdb gdb-multiarch git-lfs gitk tcl-dev tcl tcl8.6 wireshark
