sudo add-apt-repository --yes ppa:kicad/kicad-6.0-releases
sudo apt update
sudo apt install -y --install-recommends kicad
sudo apt install -y kicad-libraries kicad-packages3d kicad-symbols kicad-templates kicad-footprints
sudo apt install -y freecad
