# linux-computer-setup

Automatically install my default apps and config on Debian/Ubuntu distros

## pre script setup

The following commands are necessary to enable the script's execution:

```sh
sudo apt update
sudo apt upgrade -y
sudo apt install -y git curl
mkdir src-install
git clone https://gitlab.com/wagnmark/linux-computer-setup/
cd src-install/linux-computer-setup
sudo apt-get install flatpak
sudo apt install plasma-discover-backend-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo shutdown -r now
```
