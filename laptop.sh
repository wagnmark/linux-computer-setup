#!/bin/bash
source util-scripts/dev-setup.sh
sudo apt install -y python3 python3-pip python3-venv
source util-scripts/rust-setup.sh
source util-scripts/office-setup.sh
source util-scripts/kicad-setup.sh
