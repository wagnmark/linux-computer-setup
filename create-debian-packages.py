#!/usr/bin/env python3
import os
from pathlib import Path
import shutil
import subprocess


def clean_package_build(dir: str):
    for file in os.listdir(dir):
        if os.path.join(dir, file):
            suffix = Path(os.path.join(dir, file)).suffix
            if suffix == ".buildinfo" or suffix == ".changes" or suffix == ".deb":
                os.remove(os.path.join(dir, file))


def build_packages(srcdir: str, destdir: str):
    Path(destdir).mkdir(exist_ok=True)
    for dirname in os.listdir(srcdir):
        if os.path.isdir(os.path.join(srcdir, dirname)):
            clean_package_build(os.path.join(srcdir, dirname))
            subprocess.check_call(
                ["equivs-build", dirname], cwd=os.path.join(srcdir, dirname)
            )
            subprocess.check_call(["pwd"])
            subprocess.check_call(
                ["cp {}/*.deb {}".format(os.path.join(srcdir, dirname), destdir)],
                shell=True,
            )
            clean_package_build(os.path.join(srcdir, dirname))


if __name__ == "__main__":
    build_packages("debian-src", "debian")
